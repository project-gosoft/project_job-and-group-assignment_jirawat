package Project;

import java.util.ArrayList;
import java.util.Scanner;

public class Project_Job_Source_Code {
	static void Display(ArrayList<String> name) {
		name.add("Acer");
		name.add("Apple");
		name.add("ASUS");
		name.add("Dell");
		name.add("HUAWEI");
		name.add("Lenovo");
		name.add("Razer");
		name.add("Xiaomi");
	}

	static void Add(ArrayList<String> name) {
		Scanner reader = new Scanner(System.in);
		System.out.println("\nList Name : " + name);
		System.out.print("\nStart AddName : ");
		boolean has = false;
		String inputAdd = reader.next();
		for (String Name : name) {
			if (Name.equals(inputAdd)) {
				System.out.println("Name Sum No ADD !!!\n ");
				has = true;
			}
		}
		if (has == false) {
			name.add(inputAdd);
		}
		Print(name);
		RE(name);
	}

	static void namecom() {
		System.out.println("Select the desired menu");
		System.out.println("Add      press   	: " + "1");
		System.out.println("Delete   press   	: " + "2");
		System.out.println("Edit     press   	: " + "3");
		System.out.println("Show     press   	: " + "4");
		System.out.println("ExitProgram  press  	: " + "5");
	}

	static void Delete(ArrayList<String> name) {
		int index = 0;
		System.out.print("ID    Name");
		for (String Name : name) {
			System.out.print("\n " + index + " |  " + Name);
			index++;
		}
		Scanner reader = new Scanner(System.in);
		System.out.print("\nStart Delete : ");
		boolean has = false;
		int inputDelete;
		while (has == false)
			try {
				inputDelete = Integer.parseInt(reader.nextLine());
				name.remove(inputDelete);
				break;
			} catch (NumberFormatException nfe) {
				System.out.print("Try again: ");
			}
		has = true;
		Print(name);
		RE(name);
		has = true;

	}

	static void Print(ArrayList<String> name) {
		Scanner reader = new Scanner(System.in);
		System.out.println("\nList Name : " + name);
		System.out.print("************ Press any key to exit!!! ");
		String input = reader.next();
		System.out.println("");
	}

	static void RE(ArrayList<String> name) {
		name.remove("Acer");
		name.remove("Apple");
		name.remove("ASUS");
		name.remove("Dell");
		name.remove("HUAWEI");
		name.remove("Lenovo");
		name.remove("Razer");
		name.remove("Xiaomi");
	}

	static void Edit(ArrayList<String> name) {
		boolean has = false;
		int index = 0;
		Scanner reader = new Scanner(System.in);
		System.out.print("\nID    Name");
		for (String Name : name) {
			System.out.print("\n " + index + " |  " + Name);
			index++;
		}
		System.out.print("\nInput ID = ");
		int ID = reader.nextInt();
		has = false;

		if (ID >= name.size()) {
			System.out.print("ID No ListID!!!");
			has = true;
		}
		if (has == false) {
			System.out.print("Input Edit [ " + name.get(ID) + " ] to :");
			String name1 = reader.next();
			name.set(ID, name1);
		}
		Print(name);
		RE(name);
	}

	static void List(ArrayList<String> name) {
		int index = 0;
		System.out.println("\nGet List Name");
		System.out.print("ID    Name");
		for (String Name : name) {
			System.out.print("\n " + index + " |  " + Name);
			index++;
		}
		Print(name);
		RE(name);
	}

	static void Exit(ArrayList<String> name) {
		System.out.println("Exit Program");
		System.out.println("Thank you for trying my program");

	}
}
