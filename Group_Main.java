package Library;

import java.util.ArrayList; // ไว้เก็บข้อมูลที่บรรจุค่าข้อมูลชนิดเดียวกันไว้ ในลักษณะของแถวจำนวนแบบคงที่
import java.util.HashMap; // เป็นออบเจ็คของคลาส สำหรับเก็บข้อมูลในรูปแบบของ Key/Value
import java.util.Scanner; // เป็นคำสั่งสำหรับการรับค่าข้อมูลจากผู้ใช้งาน

public class Group_Main { // public ทุก method สามารถเข้าใช้ได้หมด
	static ArrayList<Group_Book> book = new ArrayList<>();
	static HashMap<String, String> borrow = new HashMap<String, String>();
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		// ------------header----------------
		System.out.println("--------------Welcome to the library--------------");
		System.out.println("");
		System.out.println("      Access the Library anytime, anywhere  ");
		System.out.println("");
		System.out.println("--------------------------------------------------");

		// ------------input id member----------------
		System.out.print("Please input your ID : ");
		String yourId = input.next();

		// ------------check number of idMember ----------------
		while (yourId.length() != 4) { // while เป็นคำสั่งที่ให้โปรแกรมใช้งานคำสั่งซ้ำๆ
			System.out.print("Please input your ID member 4 digits. : ");
			yourId = input.next();
		}
		String yourId2 = yourId.substring(0, 1); // ตัดคำออกเพื่อนำไปแยกประเภท user
		String yourIdUser = yourId2.toUpperCase(); // กรณีใส่พิมพ์เล็กจะแปลงเป็นพิมพ์ใหญ่ให้ทั้งหมด

		// ------------check type of user : Student,Teacher, Officer or Library staff----------------
		if (yourIdUser.equals("S")) {
			System.out.println("----------------------");
			System.out.println("You are student.");
			boolean Program = false;
			while (Program == false) {// while เพื่อให้โปรแกรมทำงานต่อเนื่องไปเรื่อยๆ
				System.out.println("");
				System.out.print(
						"Please input your number what to do : \n(1 : borrow book, 2 : return book, another number : End program) : ");
				int yourNumber = input.nextInt();
				if (yourNumber == 1) {
					borrowBook(yourId);
				} else if (yourNumber == 2) {
					returnBook(yourId);
				} else {
					System.out.println("Thank you for using library system. ");
					Program = true;
				}
			}
		} else if (yourIdUser.equals("T") || yourId2.equals("O")) {
			System.out.println("----------------------");
			System.out.println("You are teacher or officer.");
			boolean Program = false;
			while (Program == false) {
				System.out.println("");
				System.out.print(
						"Please input your number what to do : \n(1 : borrow book, 2 : return book, another number : End program) : ) : ");
				int yourNumber = input.nextInt();
				if (yourNumber == 1) {
					borrowBook(yourId);
				} else if (yourNumber == 2) {
					returnBook(yourId);
				} else {
					System.out.println("Thank you for using library system. ");
					Program = true;
				}
			}
		} else if (yourIdUser.equals("L")) {
			System.out.println("----------------------");
			System.out.println("You are library staff.");
			System.out.println("");
			System.out.print("What do you want to edit member or book ? (1: Book, 2: Member) : ");
			int needToSet = input.nextInt();
			if (needToSet == 1) {
				boolean Program = false;
				while (Program == false) {
					namecom();
					System.out.print("Please input number menu : ");
					int menu = input.nextInt();
					switch (menu) { // ใช้สำหรับเลือกการทำงานของเงื่อนไขที่ตรงกันเพียงแค่เงื่อนไขเดียวเท่านั้น
					case 1:
						System.out.println("-----add-----");
						Group_Book.addBook(book);
						break;
					case 2:
						System.out.println("-----delete-----");
						Group_Book.deleteBook(book);
						break;
					case 3:
						System.out.println("-----edit-----");
						Group_Book.editBook(book);
						break;
					case 4:
						System.out.println("-----view-----");
						Group_Book.listBook(book);
						break;
					case 5:
						System.out.println("-----exit-----");
						Group_Book.exitProgram();
						Program = true;
						break;
					}
				}
			} else if (needToSet == 2) {
				boolean Program = false;
				while (Program == false) {
					namecom();

					System.out.print("Please input number menu : ");
					int menu = input.nextInt();
					switch (menu) {
					case 1:
						Group_Member.addMember();
						System.out.println("-----add-----");
						break;
					case 2:
						Group_Member.deleteBook();
						System.out.println("-----delete-----");
						break;
					case 3:
						Group_Member.editMember();
						System.out.println("-----edit-----");
						break;
					case 4:
						Group_Member.listMember();
						System.out.println("-----view-----");
						break;
					case 5:
						Group_Member.exitProgram();
						Program = true;
						break;
					}
				}
			}
		} else {
			System.out.println("No data...");
		}
	}

	public static void borrowBook(String idMember) {
		System.out.print("Please input your name book want to borrow : ");
		String yourNameBook = input.next();
		borrow.put(idMember, yourNameBook);
		System.out.println("List book memberId " + idMember + " is borrow : " + borrow.get(idMember));
		System.out.println("");
	}

	public static void returnBook(String idMember) {
		System.out.print("Please input your name book want to return : ");
		String yourNameBookReturn = input.next();
		borrow.remove(idMember);
		System.out.println("List book memberId " + idMember + " is borrow : " + borrow.get(idMember)); // get หรือ Getter ตัวเรียกข้อมูล
		System.out.println("");
	}

	public static void namecom() {
		System.out.println("");
		System.out.println("Select the desired menu");
		System.out.println("Add      press   : " + "1");
		System.out.println("Delete   press   : " + "2");
		System.out.println("Edit     press   : " + "3");
		System.out.println("Show     press   : " + "4");
		System.out.println("Exit	 press   : " + "5");
		System.out.println("");
	}
}
